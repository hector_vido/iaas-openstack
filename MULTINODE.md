# Configuração Multinode

Em uma instalação `multi-node` do DevStack temos uma infraestrutura mais próxima a um cluster produtivo, temos uma máquina **controller**, responsável pelos serviços chave, e máquinas **compute** responsáveis pelo trabalho de virtualização. Apesar disso, ainda não temos redundância destes serviços chave.

Como a instalação `multi-node` utiliza vários nós, faça uso do comando `hostnamectl set-hostname <hostname>` para criar hostnames específicos para cada máquina se necessário, isso evita confusão em relação ao posicionamento das máquinas virtuais.

## Controller

Como o usuário `stack` executar os seguintes comandos mais abaixo na máquina **controller** como base. Para esse tipo de instalação precisamos nos atentar as configurações da rede para evitar colisões.

```bash
git clone https://opendev.org/openstack/devstack
cd devstack
git checkout stable/zed
cat > local.conf <<'EOF'

[[local|localrc]]
ADMIN_PASSWORD=openstack
DATABASE_PASSWORD=$ADMIN_PASSWORD
RABBIT_PASSWORD=$ADMIN_PASSWORD
SERVICE_PASSWORD=$ADMIN_PASSWORD
HOST_IP=10.42.0.201
FIXED_RANGE=172.24.4.0/24
FLOATING_RANGE=10.42.0.224/27
LOGFILE=/opt/stack/logs/stack.sh.log
enable_plugin heat https://opendev.org/openstack/heat stable/zed
enable_service s-proxy s-object s-container s-account
EOF

cat > /etc/systemd/system/devstack-reboot-fix.service <<'EOF'
[Unit]
Description=Sobe a bridge e configura o iptables
After=ovs-vswitchd.service

[Service]
Type=oneshot
ExecStart=ip link set br-ex up
ExecStart=ip addr add 172.24.4.1/24 dev br-ex
ExecStart=iptables -t nat -A POSTROUTING -j MASQUERADE -s 172.24.4.0/24

[Install]
WantedBy=multi-user.target
EOF
systemctl enable devstack-reboot-fix.service
```

Preste atenção no endereço IP definido em `HOST_IP` pois deve ser o endereço da máquina em questão.

## Compute

Para iniciar as máquinas **compute**, a máquina **controller** deve estar provisionada, utilizar os seguintes comandos como base:

```bash
git clone https://opendev.org/openstack/devstack
cd devstack
git checkout stable/zed
cat > local.conf <<'EOF'
[[local|localrc]]
ADMIN_PASSWORD=openstack
DATABASE_PASSWORD=$ADMIN_PASSWORD
RABBIT_PASSWORD=$ADMIN_PASSWORD
SERVICE_PASSWORD=$ADMIN_PASSWORD
#enable_plugin heat https://opendev.org/openstack/heat stable/zed
#enable_service s-proxy s-object s-container s-account
HOST_IP=10.42.0.202
FIXED_RANGE=172.24.4.0/24
FLOATING_RANGE=10.42.0.224/27
LOGFILE=/opt/stack/logs/stack.sh.log
DATABASE_TYPE=mysql
SERVICE_HOST=10.42.0.201
MYSQL_HOST=$SERVICE_HOST
RABBIT_HOST=$SERVICE_HOST
GLANCE_HOSTPORT=$SERVICE_HOST:9292
ENABLED_SERVICES=n-cpu,c-vol,placement-client,ovn-controller,ovs-vswitchd,ovsdb-server,q-ovn-metadata-agent
NOVA_VNC_ENABLED=True
NOVNCPROXY_URL="http://$SERVICE_HOST:6080/vnc_lite.html"
VNCSERVER_LISTEN=$HOST_IP
VNCSERVER_PROXYCLIENT_ADDRESS=$VNCSERVER_LISTEN
EOF
```

Preste atenção no endereço IP definido em `HOST_IP` pois deve ser o endereço da máquina em questão e em `SERVICE_HOST` pois deve ser o endereço do controller.

## Instalação

Com o arquivo definido, iniciar a instalação:

```bash
./stack.sh
```
