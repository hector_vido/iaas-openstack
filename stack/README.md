# HOT

Completar o template `lua-incompleto.yml`. O template está incompleto e alguns nomes estão errados.
A ideia é utilizar variáveis sempre que possível para tornar o template o mais genérico, variáveis podem ser utilizadas em:

- Nome da imagem da aplicação
- Nome da imagem do banco de dados
- Nome da base de dados a ser criada
- Nome do usuário da base de dados
- Nome do par de chaves SSH
- O nome do `flavor` a ser utilizado
- O nome da `network` a ser utilizada
- O nome da `subnet` a ser utilizada
- O nome da `network` externa (public)

## Avaliação

A avaliação será da seguinte forma:

- A nota será 6 se houver problema no `user data` de qualquer máquina;
- A nota será 7 se os todos os recursos forem criados mas não houver comunicação entre as máquinas;
- A nota será 8 se além dos requisitos acima, existir comunicação entre entre as máquinas:
  - Se for possível fazer um `curl` na porta 80 do servidor da aplicação;
  - Se o servidor da aplicação conseguir fazer um `curl` na porta 3306 do servidor da base de dados;
- A nota será 9 se além dos requisitos acima todas as variáveis citadas forem utilizadas;
- A nota será 10 se além dos requisitos acima a aplicação conseguir se comunicar com o banco de dados:
  - Fazer login e exibir a tela de status

### Aplicação

As duas páginas da aplicação são as seguinte:

![Login](images/app-login.png)

A página de login deverá aparecer independente da conexão com o banco. Caso a página de login não apareça, provavelmente é um problema no `user_data`. O provisionamento da máquina da aplicação pode levar alguns minutos na AWS.

Para acompanhar a configuração é possível observar algumas coisas:

- Os logs em `/var/log/cloud-init-output.log`;
- O processo `dpkg` instalação com o comando `top`;
- Se existe o arquivo `.zip` em /tmp;
- Se existe o diretório `/opt/app`;

Estes passos levam tempo, por isso a melhor fonte de informação acaba sendo o log do cloud-init.

![Dashboard](images/app-dashboard.png)

A página de status precisa da comunicação com o banco além da inicialização da aplicação. Para **iniciar a aplicação** acesse a máquina da aplicação e execute os seguintes comandos:

```bash
cd /opt/app
lapis migrate
```

O comando `lapis migrate` iniciará o banco de dados com as informações necessárias. Atente-se ao usuário e senha informado, pois eles deverão ser utilizados para o login, os usuários são:

| Nome                  | Email                     | Senha |
|-----------------------|---------------------------|-------|
| Paramahansa Yogananda | paramahansa@yogananda.in  | 123   |
| Mary Shelley          | victor@frankenstein.co.uk | 123   |

### Acessar a aplicação pela Internet

Para acessar a aplicação pela internet precisaremos criar uma regra no iptables da máquina do DevStack. O comando abaixo deve ser executado, mas o IP **172.24.4.20** deve ser substituido pelo `floating ip` da máquina da aplicação.

```bash
sudo iptables -t nat -A PREROUTING -p tcp --dport 9090 -j DNAT --to-destination 172.24.4.20:80
```

Uma vez que a regra esteja definida, podemos acessar a aplicação no endereço público da máquinada AWS na porta 9090.

Para remover a regra, caso tenhamos problemas na criação, podemos listar as regras e depois removê-las pelo seu número:

```bash
sudo iptables -t nat -nL --line-numbers
# output: 
# Chain PREROUTING (policy ACCEPT)
# num  target     prot opt source               destination         
# 1    DNAT       tcp  --  0.0.0.0/0            0.0.0.0/0            tcp dpt:9090 to:172.24.4.208:80
# 
# Chain INPUT (policy ACCEPT)
# num  target     prot opt source               destination
```

```bash
sudo iptables -t nat -D PREROUTING 1
```

## Recursos necessários

- OS::Heat::RandomString
- OS::Neutron::SecurityGroup
- OS::Neutron::Net
- OS::Neutron::Subnet
- OS::Neutron::Router
- OS::Neutron::RouterInterface
- OS::Neutron::Port
- OS::Neutron::FloatingIP
- OS::Nova::Server

**database_server user_data:**

```bash
#!/bin/bash
sed -i s/127.0.0.1/0.0.0.0/ /etc/mysql/mariadb.conf.d/50-server.cnf
cat <<EOF | mysql
CREATE DATABASE $db_name;
CREATE USER $db_user@'%' IDENTIFIED BY '$db_password';
GRANT ALL PRIVILEGES ON *.* TO $db_user@'%';
EOF
systemctl restart mysql
```

**app_server user_data:**

```bash
#!/bin/bash
apt-get update
apt-get -y install --no-install-recommends wget gnupg ca-certificates
wget -O - https://openresty.org/package/pubkey.gpg | sudo apt-key add -
echo "deb http://openresty.org/package/debian buster openresty" > /etc/apt/sources.list.d/openresty.list

apt-get update
apt-get -y install openresty luarocks lua-inspect lua-sql-mysql gcc libssl-dev liblua5.1-dev
luarocks install lapis

wget https://github.com/hector-vido/lua-ms/archive/refs/heads/master.zip -O /tmp/master.zip
unzip /tmp/master.zip -d /tmp/
mkdir -p /opt/app/
mv /tmp/lua-ms-master/* /opt/app/
sed 's/8080/80/' /opt/app/docker/nginx.conf > /etc/openresty/nginx.conf

cat > /opt/app/config.lua <<EOF
-- config.lua
local config = require('lapis.config')

config('development', {
  secret = 'UxNGNjv5qjbmhpbRjTnVNGknUEzBQnee',
  mysql = {
    host = '$db_ip',
    port = '3306',
    user = '$db_user',
    password = '$db_password',
    database = '$db_name'
  }
})
EOF
systemctl restart openresty
```
